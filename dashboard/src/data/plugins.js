export default [
    {
        avatar: 'https://logo.clearbit.com/blazeverify.com',
        description:
            'Email address verification that improves quality and deliverability.',
        title: 'Blaze Verify',
        url: 'https://blazeverify.com/',
        type: 'Enrichment',
    },
    {
        avatar: 'https://logo.clearbit.com/clearbit.com',
        description: 'Provide enriched data on the user you are talking with.',
        title: 'Clearbit',
        url: 'https://clearbit.com',
        type: 'Enrichment',
    },
    {
        avatar:
            'https://www.hubspot.com/hubfs/assets/hubspot.com/style-guide/brand-guidelines/guidelines_approved-sprocket-2.svg',
        description:
            'HubSpot is a developer and marketer of software products for inbound marketing and sales',
        title: 'HubSpot',
        url: 'https://hubspot.com',
        type: 'CRM',
    },
    {
        avatar: 'https://logo.clearbit.com/zapier.com',
        description:
            'Connect the apps you use everyday to automate your work and be more productive.',
        title: 'Zapier',
        url: 'https://zapier.com',
        type: 'I/O',
    },
    {
        avatar: 'https://logo.clearbit.com/gaiq-center.com',
        description:
            'Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.',
        title: 'Google Analytics',
        url: 'https://analytics.google.com',
        type: 'Analytics',
    },
    {
        avatar: 'https://logo.clearbit.com/slack.com',
        description: 'Convert your hottest leads right from Slack.',
        title: 'Slack',
        url: 'https://slack.com',
        type: 'CRM',
    },
    {
        avatar: 'https://logo.clearbit.com/mailchimp.com',
        description:
            'Mailchimp is an American marketing automation platform and an email marketing service.',
        title: 'Mailchimp',
        url: 'https://mailchimp.com',
        type: 'CRM',
    },
    {
        avatar: 'https://logo.clearbit.com/stripe.dev',
        description:
            'Stripe allows individuals and businesses to make and receive payments over the Internet.',
        title: 'Stripe',
        url: 'https://stripe.com',
        type: 'Payments',
    },
];
