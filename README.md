# Combase
Open Source White Label Customer Support Chat – Powered by Stream Chat 🗯️

## Deployment

Step 1: Deploy to Heroku

[![Heroku Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/GetStream/comba)

Step 2: Deploy to Netlify

[![Netlify Deploy](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/GetStream/comba)

## Resources
- https://getstream.io/blog/white-label-chat-solutions/
